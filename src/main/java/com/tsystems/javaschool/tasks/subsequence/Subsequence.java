package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        List listX = x;
        List listY = y;
        int numAccordance = 0;
        int lastPositionAccordance = 0;
        for (int i = 0; i < listX.size(); i++) {
            Object objX = listX.get(i);
            for (int j = lastPositionAccordance; j < listY.size(); j++) {
                if (objX.equals(listY.get(j))) {
                    numAccordance++;
                    lastPositionAccordance = j + 1;
                    break;
                }
            }
        }
        if (numAccordance == listX.size()) {
            return true;
        }
        return false;
    }

}
