package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (isValidStatement(statement)) {
            String polishNotation = getPolishNotationOfStatement(statement);
            try {
                double answer = round(extractNumber(polishNotation), 4);
                if (answer - ((int) answer) == 0.0) {
                    return Integer.toString((int) answer);
                }
                return Double.toString(extractNumber(polishNotation));
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    private boolean isValidStatement(String statement) {
        if (statement == null || statement.isEmpty()) {
            return false;
        }

        String lastCh = statement.substring(0, 1);
        int numOpened = 0;
        int numClosed = 0;

        for (int i = 0; i < statement.length(); i++) {
            String currCh = statement.substring(i, i + 1);
            if (" ".equals(currCh)) {
                continue;
            }
            if ("(".equals(currCh)) {
                numOpened++;
            }
            if (")".equals(currCh)) {
                numClosed++;
            }
            if (lastCh.matches("\\d")) {
                if (!(currCh.matches("\\*|/|\\+|-|\\)") || currCh.matches("\\d") || currCh.equals("."))) {
                    return false;
                }
            } else if (lastCh.equals(".")) {
                if (!currCh.matches("\\d")) {
                    return false;
                }
            } else if (lastCh.matches("\\*|/|\\+|-")) {
                if (!currCh.matches("\\d|\\(")) {
                    return false;
                }
            } else if (lastCh.equals("(")) {
                if (!currCh.matches("\\d|\\(")) {
                    return false;
                }
            } else if (lastCh.equals(")")) {
                if (!currCh.matches("\\*|/|\\+|-|\\)")) {
                    return false;
                }
            }
            lastCh = currCh;
        }
        if (numClosed == numOpened && lastCh.matches("\\d|\\)")) {
            return true;
        }
        return false;
    }

    private double extractNumber(String polishNotation) throws ArithmeticException {
        LinkedList<Double> stack = new LinkedList<>();
        for (String elem : polishNotation.split(" ")) {
            if (elem.matches("\\*|/|\\+|-")) {
                double trg;
                switch (elem) {
                    case "*":
                        stack.push(stack.pop() * stack.pop());
                        break;
                    case "/":
                        trg = stack.pop();
                        if (trg == 0.0) {
                            throw new ArithmeticException();
                        }
                        stack.push(stack.pop() / trg);
                        break;
                    case "+":
                        stack.push(stack.pop() + stack.pop());
                        break;
                    case "-":
                        trg = stack.pop();
                        stack.push(stack.pop() - trg);
                }
            } else {
                stack.push(Double.parseDouble(elem));
            }
        }
        return round(stack.pop(), 4);
    }

    private String getPolishNotationOfStatement(String statement) {
        LinkedList<String> stack = new LinkedList<>();
        StringBuilder polishNotation = new StringBuilder();

        for (int i = 0; i < statement.length(); i++) {
            String symbol = statement.substring(i, i + 1);
            if (symbol.matches("\\s")) {
                continue;
            }
            if (symbol.matches("\\.")) {
                polishNotation.replace(polishNotation.length() - 1, polishNotation.length(), ".");
                // polishNotation.insert(polishNotation.length() - 1, ".");
                continue;
            }
            if (symbol.matches("\\d")) {
                while (symbol.matches("\\d")) {
                    polishNotation.append(symbol);
                    if (i == statement.length() - 1) {
                        break;
                    }
                    i++;
                    symbol = statement.substring(i, i + 1);
                }
                if (i != statement.length() - 1) {
                    i--;
                }
                polishNotation.append(" ");
            } else if (symbol.matches("\\*|/|\\+|-")) {
                if (stack.isEmpty() || checkPriority(stack.peek()) < checkPriority(symbol)) {
                    stack.push(symbol);
                } else {
                    while (!stack.isEmpty() && checkPriority(stack.peek()) >= checkPriority(symbol)) {
                        polishNotation.append(stack.pop()).append(" ");
                    }
                    stack.push(symbol);
                }
            } else if ("(".equals(symbol)) {
                stack.push(symbol);
            } else if (")".equals(symbol)) {
                while (!"(".equals(stack.peek())) {
                    polishNotation.append(stack.pop() + " ");
                }
                stack.pop();
            }
        }

        while (!stack.isEmpty()) {
            polishNotation.append(stack.pop() + " ");
        }

        return polishNotation.toString();
    }


    private int checkPriority(String symbol) {
        switch (symbol) {
            case "*":
                return 3;
            case "/":
                return 3;
            case "+":
                return 2;
            case "-":
                return 2;
            case "(":
                return 1;
        }
        return 0;
    }

    private double round(double value, int places) { //round
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }


}
