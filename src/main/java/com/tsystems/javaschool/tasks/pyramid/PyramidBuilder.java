package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            Collections.sort(inputNumbers);
        } catch (Exception | Error e) {
            throw new CannotBuildPyramidException();
        }
        System.out.println(inputNumbers);

        int height = getPyramidHeight(inputNumbers);
        if (height == -1) {
            throw new CannotBuildPyramidException();
        }
        int resultMatrix[][] = new int[height][2 * height - 1];

        int pos = inputNumbers.size() - 1;
        int shift = 0;
        for (int i = resultMatrix.length - 1; i >= 0; i--) {
            for (int j = resultMatrix[0].length - shift - 1; j >= shift; j--) {
                resultMatrix[i][j] = inputNumbers.get(pos--);
                j--;
            }
            shift++;
        }


        return resultMatrix;
    }

    private int getPyramidHeight(List<Integer> list) {
        if (list.isEmpty()) {
            return -1;
        }
        int sum = 0;
        int member = 1;

        while (sum < list.size()) {
            sum += member++;
        }
        if (sum == list.size()) {
            return --member;
        }

        return -1;
    }

}
